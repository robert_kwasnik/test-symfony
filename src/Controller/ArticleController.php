<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ArticleController extends Controller {

  /**
   * @Route("/articles", name="article_list")
   */
  public function list() {
    $articles = $this->getArticles();

    $response = new JsonResponse(['data' => $articles]);
    $response->headers->set('Access-Control-Allow-Origin', '*');

    return $response;
  }

  protected function getArticles() {
    $articles = [];
    $date = new \DateTime('2000-01-01');

    for ($i=1; $i < 11 ; $i++) {
      $articles[] = ['id' => $i, 'author' => "Jan Kowalski " . $i, "title" => "Przykładowy tytuł " . $i, "content" => "Przykładowy kontent " . $i, "date" => $date->modify("+1 day")->format('Y-m-d')];
    }

    return $articles;
  }
}

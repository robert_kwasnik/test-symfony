<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class MenuController extends Controller {

  /**
   * @Route("/menu", name="menu_list")
   */
  public function list() {
    $menu = $this->getMenu();

    $response = new JsonResponse(['data' => $menu]);
    $response->headers->set('Access-Control-Allow-Origin', '*');

    return $response;

  }

  protected function getMenu() {
    $menu = [];

    for ($i=1; $i < 4 ; $i++) {
      $menu[] = ['id' => $i, "title" => "Odnośnik " . $i];
    }


    return $menu;
  }
}
